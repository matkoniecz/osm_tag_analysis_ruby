require_relative 'tag_analysis_by_country_for_streetcomplete.rb'
require_relative 'tag_analysis.rb'
require 'cartocss_helper'
require 'json'
require 'set'
include CartoCSSHelper

def main
  CartoCSSHelper::Configuration.set_path_to_folder_for_cache('/media/mateusz/OSM_cache/OSM-cache')

  json_overpass_list = json_overpass_list_of_countries(invalidate_cache: false)
  territories = parsed_areas_to_list_of_territories(json_overpass_list)

  #regenerate_denomination_data(territories)
  regenerate_charging_station_data(territories)
  #regenerate_atm_data(territories)



  #religion_stats_by_country()
  #tactile_paving_info()
=begin
  keys_with_cycleway_info().each do |key|
        distribution = value_distribution("Australia", "Australia", key, "").sort_by { |_key, value| -value }
        puts "#{key} "
        distribution.each do |key, value|
            puts "#{key} x#{value}" if value>=5
        end
    end
=end

#json_overpass_list_of_territories_in_area('US', 4, invalidate_cache: true)
  #bikeway_check
  #show_generic_stats_by_coutry("traffic_signals:sound", ["[crossing=traffic_signals]"])
  #show_generic_stats_by_coutry("operator", ["[amenity=charging_station]"])

  #generate_skeletons_for_initial_filters("shelter", "[highway=bus_stop]", ['CN'], ['PL'], [])
  #analyse_import_tags
end

def regenerate_atm_data(territories)
  yaml_filepath = streetcomplete_folder + '/res/country_metadata/atmOperators.yml'
  puts(yaml_filepath)
  main_key = "operator"
  filter = "[amenity=atm]"  

  #to delete specific cache
  #value_distribution("Polska", "Polska", main_key, filter, invalidate_cache: true)
  #value_distribution("Бългaрия", "Bulgaria", main_key, filter, invalidate_cache: true)
  
  recreate_streetcomplete_yaml_file_text(territories, main_key, filter, yaml_filepath)
end

def regenerate_charging_station_data(territories)
  yaml_filepath = streetcomplete_folder + '/res/country_metadata/chargingStationOperators.yml'
  puts(yaml_filepath)
  main_key = "operator"
  filter = "[amenity=charging_station]"  

  #to delete specific cache
  #value_distribution("Polska", "Polska", main_key, filter, invalidate_cache: true)
  #value_distribution("Бългaрия", "Bulgaria", main_key, filter, invalidate_cache: true)
  
  recreate_streetcomplete_yaml_file_text(territories, main_key, filter, yaml_filepath)
end

def regenerate_denomination_data(territories)
  for territory in territories
    print(territory.iso3166_code)
    print(" ")
    print(territory.name)
    print("\n")
  end
  
  puts("obtaining value_distribution_for_each_territory for denominations")
  yaml_filepath = streetcomplete_folder + '/res/country_metadata/popularChristianDenominations.yml'
  main_key = "denomination"
  filter = "[religion=christian][amenity=place_of_worship][denomination!=catholic]"
  recreate_streetcomplete_yaml_file_text(territories, main_key, filter, yaml_filepath, min_count=50)
end


def streetcomplete_yaml_file_header()
  header = ""
  header += "# Do not edit manually, if anything is wrong here it almost certainly should be fixed in OSM map data.\n"
  header += "# Data generated by counting number of OSM elements in the respective countries.\n"
  header += "\n"
end

def recreate_streetcomplete_yaml_file_text(territories, main_key, filter, yaml_filepath, min_count=2)
  raw_data = value_distribution_for_each_territory(territories, main_key, filter)
  puts("---------------------------")
  new_file = ""
  new_file += streetcomplete_yaml_file_header()
  raw_data.sort_by! { |a| a[:entry].iso3166_code }
  for territory in raw_data
    used_names_in_canonical_form = []
    sorted = territory[:stats].to_a
    sorted.sort_by! { |a| -a[1] }
    country_code_not_shown = true
    for entry in sorted
        count = entry[1]
        name = entry[0]
        if count >= min_count
            if name.include?('"')
              #next if count < min_count * 2
              puts(name)
              puts(count)
              puts(territory[:entry].name)
              puts(territory[:entry].tags['name:pl'])
              #raise '" in text, it needs escaping in yaml'
            end
            if country_code_not_shown
                new_file += territory[:entry].iso3166_code + ":\n"
                #puts(territory[:entry].name)
                #puts(territory[:entry].tags['name:pl'])
                country_code_not_shown = false
            end
            canonical = name.downcase
            if used_names_in_canonical_form.include?(canonical)
              next
            end
            used_names_in_canonical_form.push(canonical)
            if name.include?('"')
              name = '"' + name.gsub('"', '\"') + '"'
            end
            new_file += "  - " + name + " # " + count.to_s + "\n"
        end
    end
  end
  puts(new_file)
  save_streetcomplete_yaml_file(new_file, yaml_filepath)
end


def save_streetcomplete_yaml_file(text, yaml_filepath)
  puts("press enter to overwrite " + yaml_filepath)
  puts("remember to run")
  puts("./gradlew GenerateMetadataByCountry --stacktrace")
  puts("later (or maybe I should run it, with that script?)")
  gets
  File.open(yaml_filepath, 'w') { |file| file.write(text) }
  puts("saved")
end


def show_internal_australia_administrative_divisions
  json_overpass_list = json_overpass_list_of_territories_in_area("AU", 4, invalidate_cache: false)
  territories = parsed_areas_to_list_of_territories(json_overpass_list)
  for territory in territories
    print(territory.iso3166_code)
    print(territory.name)
  end
  raw_data = value_distribution_for_each_territory(territories, main_key, filter)
end

def tactile_paving_info
    hong_kong = 'CN-91'
    singapore = 'SG'
    australia = 'AU'
    new_zealand = 'NZ'
    tactile_expected = [hong_kong, singapore, australia, new_zealand]
    tactile_not_expected = []
    generate_skeletons_for_initial_filters("tactile_paving", "[highway=crossing]", ['RU', 'CN'], tactile_expected + ['PL'], tactile_not_expected)
    generate_skeletons_for_initial_filters("tactile_paving", "[highway=bus_stop]", ['RU', 'US', 'CN'], tactile_expected, tactile_not_expected)
  end

def streetcomplete_folder
    '/home/mateusz/Documents/install_moje/OSM software/StreetComplete'
end
#yml_filepath = streetcomplete_folder + '/res/country_metadata/popularReligions.yml'

def use_hacky_regexp_to_remove_part_of_file(regexp, text)
    after_regexp = text.gsub(regexp, '')
    raise "file format changed, #{regexp} failed to change anything" if text == after_regexp
    return after_regexp
end

def currently_allowed_quest_by_codes(path_from_quest_folder)
    cycleway_metadata_file = streetcomplete_folder + 'app/src/main/java/de/westnordost/streetcomplete/quests/' + path_from_quest_folder
    File.open(cycleway_metadata_file) do |file|
        text = file.read
        after_stripping_prefix = use_hacky_regexp_to_remove_part_of_file(/.*Countries.noneExcept\s*\(new String\[\]\s*{/m, text)
        after_stripping_suffix = use_hacky_regexp_to_remove_part_of_file(/\}\);.*/m, after_stripping_prefix)
        after_stripping_comments = use_hacky_regexp_to_remove_part_of_file(/\/\/.*\n/, after_stripping_suffix)
        after_stripping_whitespace = use_hacky_regexp_to_remove_part_of_file(/\s+/, after_stripping_comments)
        return after_stripping_whitespace.split(",").map {|entry| entry.gsub('"', '')}
    end
end

def world_territories
    world = json_overpass_list_of_countries()
    usa = json_overpass_list_of_territories_in_area('US', 4)
    china = json_overpass_list_of_territories_in_area('CN', 4)
    sections = [world, usa, china]
    territories = []
    sections.each do |section|
        territories += parsed_areas_to_list_of_territories(section)
    end
    return territories
end

def compare_OSM_data_and_StreetComplete_data(path_from_quest_folder, discarded_from_whitelist, classified_entries)
    classified_codes = {:whitelist => [], :blacklist => [], :testing_enabled => []}
    classified_entries.each do |_key, list|
        list.each do |entry|
            classified_codes[key] << entry[:entry].iso3166_code
        end
    end

    territories = world_territories
    currently_in_SC = currently_allowed_quest_by_codes(path_from_quest_folder)


    no_longer_on_whitelist = classified_codes[:whitelist].to_set.intersection(discarded_from_whitelist.to_set).to_a
    if no_longer_on_whitelist != []
        puts "#{no_longer_on_whitelist} is no longer necessary to be excluded, it is no longer present on whitelist"
    end
    expected_not_found = (classified_codes[:whitelist].to_set - currently_in_SC.to_set - discarded_from_whitelist.to_set).to_a
    blacklisted_found = (classified_codes[:blacklist].to_set.intersection(currently_in_SC.to_set)).to_a
    if expected_not_found != []
        puts "expected_not_found:"
        expected_not_found.each do |code|
            entry = (territories.select {|entry| entry[:entry].iso3166_code == code}).first
            puts "#{entry[:entry].iso3166_code} - #{entry[:entry].tags["name:en"]}"
        end
    end
    puts "testing_enabled: #{classified_codes[:testing_enabled]}"
    puts "blacklisted_found: #{blacklisted_found}"
end

def bikeway_check
    path_from_quest_folder = 'bikeway/AddCycleway.java'
    discarded_from_whitelist = ['MY', 'TH']
    classified_entries = bikeway_stats
    compare_OSM_data_and_StreetComplete_data(path_from_quest_folder, discarded_from_whitelist, classified_entries)
end

def format_codes_into_java_list(code_list)
    return '"' + code_list.join('", "') + '",'
end

def generate_code_for_filtering_in_quest(manual, classified_entries, main_tag)
    entries = classified_entries[:whitelist]
    entries.map! {|entry| entry[:entry].iso3166_code}
    entries.keep_if {|entry| manual.include?(entry) == false}
    tested = classified_entries[:testing_enabled].map {|entry| entry[:entry].iso3166_code}

    returned = '	@Override public Countries getEnabledForCountries()
	{
		return Countries.noneExcept(new String[]
		{
			// areas based on research
			' + format_codes_into_java_list(manual) + '
			// generated from OSM data' + '
			' + format_codes_into_java_list(entries)
    if tested != []
        returned += '
			// areas rarely tagged with ' + main_tag + ' (both yes and no value) 
			// but this tag is frequently used for other objects, 
			// so it is still useful to collect data
			// and disable quest in future if it turns out to be mostly no
			' + format_codes_into_java_list(tested)
	end
    returned += '
		});
	}'
end

def generate_skeletons_for_initial_filters(main_tag, filter, split_countries, expected, not_expected)
    data = yes_no_usage_stats(main_tag, filter, split_countries)
    present = data[:whitelist] + data[:testing_enabled]
    present.map! {|entry| entry[:entry].iso3166_code}
    missing = (expected.to_set - present.to_set).to_a
    if missing != []
        puts "#{missing} were expected but are missing"
    end
    puts generate_code_for_filtering_in_quest(expected, data, main_tag)
end

def analyse_import_tags
  area_name = "Massachusetts"
  query = '[out:json];relation[boundary=administrative][type!=multilinestring][name=' + area_name + '];out;'
  explanation = 'fetching ' + area_name + ' data'
  json_string = CartoCSSHelper::OverpassQueryGenerator.get_overpass_query_results(query, explanation)
  parsed = JSON.parse(json_string)
  tags = [
    #'massgis:IT_VALC', # short codes with unknown meaning, useless with documentation, probably useless anyway I found potential match but it was not helpful - 'Wetland label abbreviations' as listed in http://www.mass.gov/anf/research-and-tech/it-serv-and-support/application-serv/office-of-geographic-information-massgis/datalayers/wetchange.html 
    #'massgis:way_id', # 99% are numbers, other cases are ;-separated numbers - useless id
    #'massgis:INTSYM', #always value is "CR" or "APR"
    #'massgis:OLI_1_ORG', #human readable values
    #'massgis:OLI_2_ORG', #human readable values
    'massgis:OLI_3_ORG',
    #'massgis:OLI_1_TYPE', values are always "S", "L" or "M"
    #'massgis:OLI_2_TYPE', #values are always "S", "L" or "M"
    'massgis:OLI_3_TYPE',
    #'massgis:OLI_1_ABRV', #some text codes, without obvious meaning
    #'massgis:OLI_2_ABRV', #some text codes, without obvious meaning
    'massgis:OLI_3_ABRV',
    #'massgis:ASSESS_BLK', #'Local Assessor’s Block.' - sounds useless, YA another internal id

    #'massgis:SHAPE_LEN', #numbers
    #'massgis:GRANTTYPE1', #values are always "S" or "F"
    #'massgis:GRANTTYPE2', #values are always "S" or "F"
    'massgis:GRANTPROG1',
    'massgis:GRANTPROG2',
    'massgis:SOURCE_TYP',
    #'massgis:SOURCE_ACC', #value is always "3"
    'massgis:CAL_DATE_R',
    'massgis:ASSESS_SUB',
    'massgis:PROJ_ID1', # yet another internal id
    'massgis:PROJ_ID2',
    'massgis:LOC_ID', # 'Link to MassGIS Standard Parcel data (LOC_ID).' - yet another useless id
    'massgis:geom_id',
    #'massgis:OLI_1_INT', #always value is "CR" or "APR"
    'massgis:OLI_2_INT',
    'massgis:OLI_3_INT',
    'massgis:BOND_ACCT',
    'massgis:MANAGR_ABR',
    #'massgis:MANAGR_TYP', value is always "M"
    'massgis:UseType',
    'massgis:Sequence',
    'massgis:PicLink',
    'massgis:HistLink',
    'massgis:TypeCode',
    'massgis:GridNum',
    'massgis:NameOffc',
    'massgis:BanCod',
    'massgis:Address',
    'massgis:FAACS',
    'massgis:StName',
    'massgis:StType',
    'massgis:id',
    'massgis:NameFamil',
    'massgis:PROJ_ID3',
    'massgis:Name',
    'massgis:TOTAL',
    'massgis:TIME_',
    'massgis:DAY_',
    'massgis:OBJECTID_1',
    'massgis:OBJECTID_2',
    'massgis:Shape_Le_1',
    'massgis:HNC',
    'massgis:F_S_D',
    'massgis:S_V',
    'massgis:RES',
    'massgis:UNLD',
    'massgis:MC',
    'massgis:F_S',
    'massgis:OFC',
    'massgis:PER',
    'massgis:OID_',
    'massgis:Shape_Area',
    'massgis:Shape_Leng',
    'massgis:VIS',
    'massgis:StNumsfx',
    'massgis:Type',
    'massgis:Surface',
    'massgis:NAME_1',
    'massgis:DisplayNam',
    'massgis:StPrfx',
    'massgis:town_id',
    'massgis:fourcolor',
    'massgis:landuse',
    'massgis:MAP_ID',
    'massgis:PWSID',
    'massgis:SITE_ADDR',
    'massgis:PALIS_ID',
    'massgis:OBJECTID',
    'massgis:POLY_CODE',
    'massgis:SOURCE_SCA',
    'massgis:DCAM_ID',
    'massgis:ARTICLE97',
    'massgis:POLY_ID',
    ]
=begin
  
    tags.each do |key|
      query = filter_across_named_region("['#{key}']", area_name)
      explanation = "analysis of #{key} in #{area_name}"
      puts "obtaining raw string - #{explanation}"
      json_string = CartoCSSHelper::OverpassQueryGenerator.get_overpass_query_results(query, explanation)
    end
=end

  tags.each do |tag|
    value_distribution_for_each_territory(parsed, tag).each do |entry|
      show_stats(entry[:stats], tag + " in " + area_name)
    end
  end
end

main
