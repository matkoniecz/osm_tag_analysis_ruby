#!/usr/bin/ruby

require "open-uri"
require 'json'
require 'pp'

MIN_COUNT = 3
EXCEPTIONS = [
  "FIXME",
  "unsigned",
  "none",
]

data = URI.parse('https://taginfo.openstreetmap.org/api/4/key/values?key=maxweight&sortname=count&sortorder=desc').read
data = JSON.parse(data)["data"]

# Get an array of values that only includes values with more than MIN_COUNT occurrences
counted = data.select { |h| h["count"] > MIN_COUNT }.map { |h| h["value"] }
# Discard values with ;
single_values = counted.reject { |h| h.include?(";") }
# Filter out empty strings
no_empty = single_values.reject { |h| h.strip.empty? }

# Filter out exceptions in EXCEPTIONS
filtered = no_empty - EXCEPTIONS

filtered.each do |entry|
  if entry =~ /\A[0-9]*(|\.[0-9]+)( ?t|)\Z/i
    next
  else
    puts entry
  end
end
