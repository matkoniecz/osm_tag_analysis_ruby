require 'cartocss_helper'
require_relative 'tag_analysis.rb'

def main
    CartoCSSHelper::Configuration.set_path_to_folder_for_cache('/media/mateusz/5bfa9dfc-ed86-4d19-ac36-78df1060707c/OSM-cache')
    highway_values = ["motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link", "secondary", "secondary_link", "tertiary", "tertiary_link", "unclassified", "residential", "living_street", "service", "track", "pedestrian", "road"]

    limits = ["", "['foot'!='no']", "['turn:lanes'!~'.*']", '[motorroad != yes]', '[lanes!=2][lanes!=3][lanes!=4][lanes!=5][lanes!=6]',
    "['foot'='no']", "['turn:lanes']", '[motorroad = yes]', '[lanes][lanes!=1]']
    base = {}
    limits.each do |limit|
      puts limit
      highway_values.each do |road|
          query = bridge_query("['highway'='#{road}']" + limit)
          json_data_string = CartoCSSHelper::OverpassQueryGenerator.get_overpass_query_results(query, "#{road} bridges count")
          obj = JSON.parse(json_data_string)
          all_bridge_segments = obj["elements"][0]["tags"]["total"]

          query = bridge_query("['highway'='#{road}'][maxweight][maxweight!=none][maxweight!=unsigned]" + limit)
          json_data_string = CartoCSSHelper::OverpassQueryGenerator.get_overpass_query_results(query, "#{road} bridges with maxweight count")
          obj = JSON.parse(json_data_string)
          limited_weight_bridge_segments = obj["elements"][0]["tags"]["total"]
          percent = limited_weight_bridge_segments.to_f / all_bridge_segments.to_i * 100
          if limit == ""
            base[road] = percent
          end
          compare = percent.to_f / base[road] * 100
          if not compare.nan?
            compare = compare.to_i
          end
          if all_bridge_segments.to_i == 0
            puts "there are no bridges of this type"
          else
            puts "#{road} #{percent.to_i}.#{(percent*10).to_i%10}% (#{compare}%)"
          end
      end
      puts
      puts
    end
end

def bridge_query(filter)
    return "[out:json][timeout:1000];
        (
        way#{filter}['bridge']['bridge'!='no'];
        );
        out count;"
end

def germany_bridge_query(filter)
        return "
        [out:json][timeout:1000];
        area['name:pl'='Niemcy']->.searchArea;
            (
            way#{filter}['bridge']['bridge'!='no'](area.searchArea);
            );
            out count;"
end

main()