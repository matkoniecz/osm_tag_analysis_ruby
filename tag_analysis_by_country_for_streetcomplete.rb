require_relative 'tag_analysis.rb'
require 'cartocss_helper'
require 'set'

# used by test.rb

def religion_stats_by_country
  #ReligiousDataBuilder.produce_religious_data("[amenity=place_of_worship]", 'religion')
  #ReligiousDataBuilder.produce_religious_data("[historic=wayside_shrine]", nil) # better results are with minimal_count dropped to 5
end

module ReligiousDataBuilder
  def self.expected_religion_values
    ['bahai', 'buddhist', 'caodaism', 'christian', 'confucian', 'hindu', 'jain', 'jewish', 'muslim', 'shinto', 'sikh', 'taoist']
  end

  def self.rare_valid_ignored_religion_values
    ['voodoo', 'unitarian_universalist', 'spiritualist', 'pagan', 'scientologist', 'shamanic']
  end

  def self.invalid_or_special_values
    return ['catholic', 'none', 'multifaith']
  end

  def self.produce_religious_data(filter, main_key)
    if main_key != "religion"
      raise "this is likely not going to work"
    end
    puts add_rare_supported_religions([])
    puts
    puts add_rare_supported_religions(['christian', 'muslim'])
    puts
    puts
    puts "========"
    puts filter
    puts
    data = {}
    territories = parsed_areas_to_list_of_territories(json_overpass_list_of_countries)
    raw_religion_data = value_distribution_for_each_territory(territories, main_key, filter)
    show_global_data(raw_religion_data)
    show_country_specific_data(raw_religion_data)
  end

  def self.raise_on_unexpected_religions_in_global_data(global_data)
    global_data.each do |religion, count|
      if rare_valid_ignored_religion_values.include?(religion)
        next
        #it would be preferable to have option to detect that religion tag got more popular...
        #if count < 8 * minimal_count
        #  next
        #end
      end
      unless expected_religion_values.include?(religion)
        raise "unexpected religion value <#{religion}>"
      end
    end
  end

  def self.show_global_data(raw_religion_data)
    global_list = select_entry_for_global_data(raw_religion_data)
    raise_on_unexpected_religions_in_global_data(global_list)
    global_list = add_rare_supported_religions(global_list)
    percent_as_text = "#{(coverage_ratio*100).to_i}%"
    percent_as_text = "#{(coverage_ratio*100)}%" if (coverage_ratio * 1000)%10 != 0
    puts "// worldwide usage, values covering #{percent_as_text} of used tags"
    output_global_data_to_screen(global_list)
  end

  def self.add_rare_supported_religions(global_list)
    rare_but_supported = (expected_religion_values.to_set - global_list.to_set).to_a
    global_list += rare_but_supported
    return global_list
  end

  def self.religion_tag_value_to_streetcomplete_value(religion)
    if religion == "caodaism"
      # handles mismatch between tag value and internal SC name
      # see also https://github.com/westnordost/StreetComplete/issues/801
      return "caodaist"
    end

    return religion
  end

  def self.output_global_data_to_screen(global_list)
    global_list.each do |religion|
      next if rare_valid_ignored_religion_values.include?(religion)

      streetcomplete_religion_name = religion_tag_value_to_streetcomplete_value(religion)
      puts '      new Item("' + religion + '",      R.drawable.ic_religion_' + streetcomplete_religion_name + ',      R.string.quest_religion_' + streetcomplete_religion_name + '),'
    end
  end

  def self.raise_on_unexpected_religions_for_countries(data_for_countries)
    data_for_countries.each do |_key, data|
      popular = data[:list_of_dominating]
      unexpected = popular.to_set - expected_religion_values.to_set
      unexpected.each do |religion|
        count = data[:stats][religion]
        if rare_valid_ignored_religion_values.include?(religion)
          next if count < 8 * minimal_count
        end
        raise "unexpected religion value <#{unexpected.to_a}> used #{count} times in #{data[:label]}"
      end
    end
  end

  def self.country_specific_list_is_needed(country_list, global_list)
    country_list.each_with_index do |religion, index|
      return true if index >= global_list.length
      return true if religion != global_list[index]
    end
    return false
  end

  def self.show_country_specific_data(raw_religion_data)
    global_list = select_entry_for_global_data(raw_religion_data)
    data_for_countries = select_entry_for_countries(raw_religion_data)
    raise_on_unexpected_religions_for_countries(data_for_countries)
    codes = []
    data_for_countries.each do |_key, data|
      codes << data[:iso3166_code]
    end
    codes.sort!

    data_for_countries.each do |_key, data|
      rare_valid_ignored_religion_values.each do |religion|
        data[:list_of_dominating].delete(religion)
      end
    end
    topc_description_in_plural = "religions"

    global_list = select_entry_for_global_data(raw_religion_data)
    data_for_countries.each do |_key, data|
      next if data[:list_of_dominating].empty?
      next if data[:list_of_dominating][-1] != global_list[0]

      # last entry in list of dominating is the same as first in global
      # so it is redundant
      data[:list_of_dominating].delete(global_list[0])
    end

    puts yml_file_header(topc_description_in_plural)
    codes.each do |code|
      popular = data_for_countries[code][:list_of_dominating]
      if country_specific_list_is_needed(popular, global_list)
        puts "#{code}: [#{popular.join(", ")}]" if popular != []
      end
    end
  end

  def self.yml_file_header(what)
    return "# #{what} that should be shown first before the order defined in the form.
# no total order required. Just mention the #{what} that should show within the first few entries
default: []
"
  end

  def self.select_entry_for_global_data(raw_data)
    stats_array = []
    raw_data.each do |entry|
      stats_array << entry[:stats]
    end
    merged = Filter::merge_list_of_stats_into_one(stats_array)
    merged = Filter::drop_unwanted_values_from_stats(merged, invalid_or_special_values)
    return Filter::select_enough_values_to_cover(merged, coverage_ratio, minimal_count)
  end

  def self.select_entry_for_countries(raw_data)
    data_for_countries = {}
    raw_data.each do |entry|
      location_description = entry[:english_name] + " (" + entry[:iso3166_code] + ")"
      stats = Filter::drop_unwanted_values_from_stats(entry[:stats], invalid_or_special_values)
      reduced_to_popular = Filter::select_enough_values_to_cover(stats, local_coverage_ratio, minimal_count)
      data = {}
      data[:list_of_dominating] = reduced_to_popular
      data[:iso3166_code] = entry[:iso3166_code]
      data[:label] = location_description
      data[:stats] = entry[:stats]
      data_for_countries[data[:iso3166_code]] = data
    end
    return data_for_countries
  end

  def self.show_data(filtered, main_key, filter)
    filtered.each do |entry|
      location_description = entry[:english_name] + " (" + entry[:iso3166_code] + ")"
      info = stats_description(location_description, main_key, filter)

      puts
      puts info
      entry[:stats].each do |value, count|
        puts "#{value} x#{count}"
      end
    end
  end

  def self.coverage_ratio
    0.9995
  end

  def self.local_coverage_ratio
    0.99
  end

  def self.minimal_count
    50
  end
end

module Filter
  def self.select_enough_values_to_cover(stats, ratio, minimal_count)
    stats = drop_rare_values_from_stats(stats, minimal_count)
    total_count = 0
    stats.each do |entry|
      total_count += entry[1]
    end
    returned = []
    stats = sort_stats(stats)
    covered_count = 0
    stats.each do |value, count|
      break if covered_count > total_count * ratio

      returned << value
      covered_count += count
    end
    return returned
  end

  def self.drop_rare_values(raw_data, minimal_count)
    returned = []
    raw_data.each do |entry|
      stats = drop_rare_values_from_stats(entry[:stats], minimal_count)
      next if stats == {}

      returned << entry
      returned[-1][:stats] = stats
    end
    return returned
  end

  def self.drop_unwanted_values_from_stats(stats, unwanted_values)
    unwanted_values.each do |value|
      stats.delete(value)
    end
    return stats
  end

  def self.drop_rare_values_from_stats(stats, minimal_count)
    stats = sort_stats(stats)
    new_stats = {}
    stats.each do |value, count|
      if count >= minimal_count
        new_stats[value] = count
      end
    end
    return new_stats
  end

  def self.sort_stats(stats)
    sorted = stats.to_a
    sorted.sort_by! { |a| -a[1] }
    stats = {}
    sorted.each do |entry|
      stats[entry[0]] = entry[1]
    end
    return stats
  end

  def self.merge_list_of_stats_into_one(stats_array)
    merged = {}
    stats_array.each do |stats|
      stats.each do |row|
        what = row[0]
        count = row[1]
        merged[what] ||= 0
        merged[what] += count
      end
    end
    return merged
  end
end

def stats_description(location_description, main_key, filter)
  filter_description = "#{main_key} on #{filter}"
  filter_description = "#{main_key}" if filter == ""
  return filter_description + " in " + location_description
end

def show_generic_stats_by_coutry(main_key, filters)
  filters.each do |filter|
    territories = parsed_areas_to_list_of_territories(json_overpass_list_of_countries)
    value_distribution_for_each_territory(territories, main_key, filter).each do |entry|
      info = stats_description(entry[:entry].description, main_key, filter)
      show_stats(entry[:stats], info)
    end
  end
end

def yes_no_usage_stats(main_key, filter, split_countries, show_info_for_each_country: false)
  returned = { whitelist: [], blacklist: [], testing_enabled: [] }

  territories = parsed_areas_to_list_of_territories(json_overpass_list_of_countries)
  split_countries.each do |code|
    territories += parsed_areas_to_list_of_territories(json_overpass_list_of_territories_in_area(code, 4))
    if code != 'RU'
      territories += parsed_areas_to_list_of_territories(json_overpass_list_of_territories_in_area(code, 3))
    end
    territories.delete_if { |territory| territory.iso3166_code == code }
  end

  data_without_filter = value_distribution_for_each_territory(territories, main_key, filter)
  value_distribution_for_each_territory(territories, main_key, filter).each do |entry|
    location_description = entry[:entry].description
    info = stats_description(location_description, main_key, filter)
    if show_info_for_each_country
      puts
      puts info
      show_yes_no_stats(entry[:stats])
    end
    classified = classify_entry(entry, data_without_filter)
    next if classified.nil?

    returned[classified] << entry
  end
  show_segregated_lists(returned)
  return returned
end

def classify_entry(entry, data_without_filter)
  yes = yes_no_stats(entry[:stats])[:yes]
  no = yes_no_stats(entry[:stats])[:no]
  return nil if yes + no == 0
  percent = yes*100/(yes+no)
  if yes > 100 && percent >= 20
    return :whitelist
  elsif no > 100 && percent < 8
    return :blacklist
  elsif
    unfiltered_entry = (data_without_filter.select {|unfiltered_entry| unfiltered_entry[:iso3166_code] == entry[:iso3166_code]}).first
    yes = yes_no_stats(unfiltered_entry[:stats])[:yes]
    no = yes_no_stats(unfiltered_entry[:stats])[:no]
    return nil if yes + no == 0
    percent = yes*100/(yes+no)
    if yes > 100 && percent >= 20
      return :testing_enabled
    end
  end
  return nil
end

def bikeway_stats()
  country_codes_with_detailed_analysis = ['US', 'CN']
  returned = []
  country_codes_with_detailed_analysis.each do |code|
    returned << bikeway_stats_by_area_in_area(code, 4)
    if code != 'RU'
      returned << bikeway_stats_by_area_in_area(code, 3)
    end
  end
  world_data = bikeway_stats_by_country()

  country_codes_with_detailed_analysis.each do |code|
    world_data[:whitelist].delete_if {|entry| entry[:entry].iso3166_code == code }
    world_data[:blacklist].delete_if {|entry| entry[:entry].iso3166_code == code }
  end
  returned << world_data

  merged_stats = {}
  returned.each do |data|
    data.each do |key, value|
      if merged_stats.include?(key)
        merged_stats[key] += value
      else
        merged_stats[key] = value
      end
    end
  end
  return merged_stats
end

def bikeway_stats_by_country
  bikeway_stats_by_territory_group(json_overpass_list_of_countries)
end

def bikeway_stats_by_area_in_area(iso3166_code, admin_level)
  bikeway_stats_by_territory_group(json_overpass_list_of_territories_in_area(iso3166_code, admin_level))
end

def keys_with_cycleway_info
  return ["cycleway:both", "cycleway", "cycleway:left", "cycleway:right"]
end

def bikeway_stats_by_territory_group(territory_areas)
  # currently enabled
  # https://github.com/westnordost/StreetComplete/blob/f4aa38fa48d2408835f563db246a6b3e9665657d/app/src/main/java/de/westnordost/streetcomplete/quests/bikeway/AddCycleway.java#L201
  filters = [""]
  filters.each do |filter|
    keys = keys_with_cycleway_info
    data = {}
    merged_data_for_each_territory = {}
    keys.each do |key|
      territories = parsed_areas_to_list_of_territories(territory_areas)
      data = value_distribution_for_each_territory(territories, key, filter)
      data.each do |entry|
        key = entry[:iso3166_code] # entry[:english_name] + " (" + entry[:iso3166_code] + ")"
        merged_data_for_each_territory[key] ||= {}
        entry[:stats].each do |value, count|
          merged_data_for_each_territory[key][value] ||= 0
          merged_data_for_each_territory[key][value] += count
        end
      end
    end

    blacklist = []
    whitelist = []

    merged_data_for_each_territory.each do |code, stats|
      yes = yes_no_stats(stats)[:other] + yes_no_stats(stats)[:yes]
      no = yes_no_stats(stats)[:no]
      next if yes + no == 0

      percent = yes * 100 / (yes + no)
      if yes > 25 && percent >= 20
        whitelist << entry
      elsif no > 100 && percent < 8
        blacklist << entry
      end

      filter_description = "#{keys} on #{filter}"
      filter_description = "#{keys}" if filter == ""
      show_yes_no_stats(stats)
    end

    returned = {:whitelist => whitelist, :blacklist => blacklist, :testing_enabled => []}
    show_segregated_lists(returned)
    return returned
  end
end

def show_segregated_lists(segregated)
    puts
    puts "whitelist (frequently used tag, at least sometimes used to indicate that feature is present):"
    for entry in segregated[:whitelist]
      puts "- #{entry[:entry].description}"
    end
    puts
    puts "blacklist (tag used - overwhelmingly often to indicate that feature is not present):"
    for entry in segregated[:blacklist]
      puts "- #{entry[:entry].description}"
    end
    puts
    puts
    puts "testing_enabled (tag is not classified to either of categories above, but this key is used to indicate that this feature is present on other objects):"
    for entry in segregated[:testing_enabled]
      puts "- #{entry[:entry].description}"
    end
    puts
    puts
end


def yes_no_stats(stats)
  yes = stats["yes"]
  no = stats["no"]
  yes = 0 if yes == nil
  no = 0 if no == nil
  other = 0
  stats.each do |value, count|
    if value != "yes" and value != "no"
      other += count
    end
  end
  return {yes: yes, no: no, other: other}
end

def show_yes_no_stats(stats)
  yes = yes_no_stats(stats)[:yes]
  no = yes_no_stats(stats)[:no]
  other = yes_no_stats(stats)[:other]
  if yes + no < other * 10
    return false
  end
  total = yes + no + other
  if total == 0
    return false
  end
  puts "yes: #{yes*100/total}%"
  puts "no: #{no*100/total}%"
  puts "other: #{other*100/total}%"
  puts stats
  return true
end