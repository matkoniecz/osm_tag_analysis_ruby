require 'overhelper'

# used for example by tag_analysis_by_country_for_streetcomplete.rb

def value_distribution(region_name, readable_region_name, key, filter, invalidate_cache: false)
  query = ComplexQueryBuilder::filter_across_named_region("['#{key}']"+filter, region_name)
  explanation = "analysis of #{key} in #{readable_region_name} for #{filter}"
  print(query)
  begin
    json_data_string = CartoCSSHelper::OverpassQueryGenerator.get_overpass_query_results(query, explanation, invalidate_cache: invalidate_cache)
  rescue CartoCSSHelper::OverpassDownloader::OverpassRefusedResponse => e
    puts "((((((((((((((((((((((("
    puts "OverpassRefusedResponse exception"
    puts e
    raise e
  end
  return count_occurences_of_tag(json_data_string, key)
end

def count_occurences_of_tag(json_data_string, key)
  obj = JSON.parse(json_data_string)
  elements = obj["elements"]
  stats = {}
  elements.each do |entry|
    if entry["tags"] != nil
      if entry["tags"][key] != nil
        value = entry["tags"][key]
        stats[value] ||= 0
        stats[value] += 1
      end
    end
  end
  return stats
end

def json_overpass_list_of_countries(invalidate_cache: false)
  query = '[out:json];relation["admin_level"="2"][boundary=administrative][type!=multilinestring];out tags;'
  explanation = 'list of countries'
  json_string = CartoCSSHelper::OverpassQueryGenerator.get_overpass_query_results(query, explanation, invalidate_cache: invalidate_cache)
  parsed = JSON.parse(json_string)
  return parsed
end

def json_overpass_list_of_territories_in_area(iso_code_of_parent_area, admin_level_of_child_area, invalidate_cache: false)
  query = "[out:json][timeout:3600];
area
  ['ISO3166-1'='#{iso_code_of_parent_area}'] -> .parent_area;
(
  rel
    (area.parent_area)
    [admin_level=#{admin_level_of_child_area}][boundary=administrative];
);
out tags;"
  explanation = "list of admin_level=#{admin_level_of_child_area} for 'ISO3166-1'='#{iso_code_of_parent_area}'"
  json_string = CartoCSSHelper::OverpassQueryGenerator.get_overpass_query_results(query, explanation, invalidate_cache: invalidate_cache)
  parsed = JSON.parse(json_string)
  return parsed
end

class Territory
  def initialize(element_from_parsed_json)
    @tags = element_from_parsed_json["tags"]
    @id = element_from_parsed_json["id"]
    @type = element_from_parsed_json["type"]
  end

  attr_reader :iso3166_code, :tags, :id, :type

  def name
    name = @tags["name:en"]
    return name if name != nil
    return nil if [7567187, 7534382].include?(@id) # known issues with entry in TODO or opened notes
    puts "no English name for #{url}"
    name = @tags["name"]
    return name if name != nil
    puts "no name for #{url}"
    return nil
  end

  def iso3166_code
    tags = ["ISO3166-1", "ISO3166-1:alpha2", "ISO3166-2"]
    tags.each do |tag|
      code = @tags[tag]
      return code if code != nil
    end
    return nil if [
      5220687, # fixed in OSM in 2020 - blatantly wrong admin level removed
      8482018, # neutered in OSM in 2020 - still requires fix but no longer described as a country
      3263728, # https://www.openstreetmap.org/note/2338627
    ].include?(@id) # known issues with entry in TODO or opened notes
    if @id != 5441968 # https://www.openstreetmap.org/changeset/111785011
      puts "no ISO3166 code for #{url} - tried reading following tags: #{tags}"
    end
    return nil
  end

  def url
    "http://www.openstreetmap.org/#{@type}/#{@id}"
  end

  def description
    name = @tags["name:en"]
    name = 'nil' if name == nil
    code = iso3166_code
    code = 'nil' if code == nil
    return name + " (" + code + ")"
  end
end

def sanity_check_of_territories(parsed_json_with_territories)
  territories_json = parsed_json_with_territories["elements"]
  territories = []
  territories_json.each do |json|
    territories << Territory.new(json)
  end
  who_has_code = {}
  territories.each do |entry|
    area_code = entry.iso3166_code
    if who_has_code[area_code] != nil && area_code != nil
      puts "duplicate: #{area_code} has both #{entry.url} and #{who_has_code[area_code]}"
    end
    who_has_code[area_code] = entry.url
  end
end


def parsed_areas_to_list_of_territories(parsed_areas)
  sanity_check_of_territories(parsed_areas)
  territories = parsed_areas["elements"]
  returned = []
  territories.each do |entry|
    territory = Territory.new(entry)
    returned << territory if territory.iso3166_code != nil 
  end
  return returned
end

def value_distribution_for_each_territory(list_of_territories, key, filter="", show_progress=false)
  list_of_stats = []
  list_of_territories.each do |entry|
    stats = value_distribution(entry.tags["name"], entry.name, key, filter)
    if show_progress
      for pair in stats.keys()
        if stats[pair] > 1
          puts("#{pair} #{stats[pair]}")
        end
      end
    end
    list_of_stats << {stats: stats, english_name: entry.name, iso3166_code: entry.iso3166_code, entry: entry}
    #return list_of_stats if list_of_stats.length > 3
  end
  return list_of_stats
end

def show_empty_stats(stats)
  if stats.length != 0
    return false
  end
  puts "tag is not present in this area"
  return true
end

def categorize_using_regexp(stats, list_of_patterns)
  returned = {}
  returned[:match_count] = 0
  returned[:match] = {}
  returned[:other_count] = 0
  returned[:other] = {}
  stats.each do |value, count|
    match = false
    list_of_patterns.each do |pattern|
      match = true if value =~ pattern
    end
    if match
      returned[:match_count] += count
      returned[:match][value] = count
    else
      returned[:other_count] += count
      returned[:other][value] = count
    end
  end
  return returned
end

def show_numbers_stats(stats)
  list_of_patterns = [/^\d+$/, /^\d+\.\d+$/, /^\d+,\d+$/]
  data = categorize_using_regexp(stats, list_of_patterns)
  numbers_count = data[:match_count]
  other_count = data[:other_count]
  if numbers_count < other_count * 10
    return false
  end
  puts "#{numbers_count*100/(numbers_count+other_count)}% are numbers"
  if data[:other] != {}
    puts "exceptions:"
    puts data[:other]
  end
  return true
end

def show_alphanumeric_soup_stats(stats)
  soup = {}
  soup_count = 0
  other = {}
  other_count = 0
  stats.each do |value, count|
    nonmuber_count = 0
    number_count = 0
    value.each_char do |letter|
      if ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].include?(letter)
        number_count += 1
      else
        nonmuber_count += 1
      end
    end
    if number_count > 0 && nonmuber_count <= 1
      soup[value] = count
      soup_count += count
    else
      other[value] = count
      other_count += count
    end
    if soup_count < other_count * 10
      return false
    end
    puts "#{soup_count*100/(soup_count+other_count)}% are mix of number(s) with at most one other character"
    if other != {}
      puts "exceptions:"
      puts other
    end
    return true
  end
end

def show_limited_values_stats(stats, values_count)
  total_count = 0
  stats.each do |key, count|
    total_count += count
  end

  count_of_top_ones = 0

  sorted = stats.to_a
  sorted.sort_by! { |a| -a[1] }
  (0..values_count - 1).each do |i|
    count_of_top_ones += sorted[i][1]
  end

  if count_of_top_ones < (total_count - count_of_top_ones) * 10
    return false
  end
  (0..values_count-1).each do |i|
    puts "#{sorted[i][0]} x#{sorted[i][1]}"
    stats.delete(sorted[i][0])
    count_of_top_ones += sorted[i][1]
  end
  if stats != {}
    puts "exceptions:"
    puts stats
  end
  return true
end

def date_in_evil_american_format(stats)
  #MM/DD/YYYY
  mmddyyyy_leading_zero = /(01|02|03|04|05|06|07|08|09|10|11|12)\/\d\d\/\d\d\d\d/
  mmddyy = /(1|2|3|4|5|6|7|8|9|10|11|12)\/(\d|\d\d)\/\d\d\d\d/
  list_of_patterns = [mmddyyyy_leading_zero, mmddyy]
  description = "dates in evil american format (MM/DD/YYYY or mm/dd/yyyy)"
  data = categorize_using_regexp(stats, list_of_patterns)
  if data[:match_count] < data[:other_count] * 10
    return false
  end
  puts "#{data[:match_count]*100/(data[:match_count] + data[:other_count])}% are #{description}"
  if data[:other] != {}
    puts "exceptions:"
    puts data[:other]
  end
  return true
end

def show_stats(stats, description)
  puts
  puts description
  return if show_empty_stats(stats)

  return if show_limited_values_stats(stats, 1)
  return if show_limited_values_stats(stats, 2)
  return if show_limited_values_stats(stats, 3)
  return if show_limited_values_stats(stats, 4)
  return if show_numbers_stats(stats)
  return if show_alphanumeric_soup_stats(stats)

  puts stats
end
