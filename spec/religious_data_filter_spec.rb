# frozen_string_literal: true

require_relative '../tag_analysis_for_streetcomplete.rb'
require 'spec_helper'

class UnexpectedRidiculousExceptions < StandardError
end

describe ReligiousDataBuilder do
  it "initializes" do
    expect(RestClientWrapper.new)
  end

  it "works for happy case" do
    a = RestClientWrapper.new
    data = "data"
    allow(RestClient::Request).to receive(:execute).and_return(data)
    expect(a.fetch_data_from_url("url", 1)).to eq data
  end

  it "raises BuggyRestClient on ArgumentError from rest-client caused by https://github.com/rest-client/rest-client/issues/359" do
    a = RestClientWrapper.new
    allow(RestClient::Request).to receive(:execute).and_raise(ArgumentError)
    expect { a.fetch_data_from_url("url", 1) }.to raise_error BuggyRestClient
  end
end
